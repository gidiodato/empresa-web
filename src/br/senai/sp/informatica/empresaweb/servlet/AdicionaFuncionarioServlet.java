/**
 * @author Giovanna Ferreira Diodato
 */

package br.senai.sp.informatica.empresaweb.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresaweb.dao.FuncionarioDao;
import br.senai.sp.informatica.empresaweb.model.Funcionario;

@WebServlet("/adicionaFuncionario")
public class AdicionaFuncionarioServlet extends HttpServlet {

	// sobrescreve o m�todo service
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// adiciona um PrintWriter
		PrintWriter leitor = res.getWriter();

		// pega os par�metros da requisi��o
		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		String cpf = req.getParameter("cpf");
		String senha = req.getParameter("senha");

		// cria um funcionario com os par�metros do formulario
		Funcionario funcionario = new Funcionario();
		funcionario.setNome(nome);
		funcionario.setEmail(email);
		funcionario.setCpf(cpf);
		funcionario.setSenha(senha);

		// abre uma conex�o com o banco de dados
		FuncionarioDao funcionarioDao = new FuncionarioDao();

		// salva o funcionario no banco de dados
		funcionarioDao.salva(funcionario);

		// mostra para o usu�rio que o contato foi adicionado para o usu�rio

		// Cria um request dispatcher
		RequestDispatcher dispatcher = req.getRequestDispatcher("/funcionario-adicionado.jsp");

		// encaminha o usuario para essa p�gina
		dispatcher.forward(req, res);

	}
}
