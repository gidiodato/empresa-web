/**
 * @author Giovanna Ferreira Diodato
 */
package br.senai.sp.informatica.empresaweb.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresaweb.dao.FuncionarioDao;
import br.senai.sp.informatica.empresaweb.model.Funcionario;


public class SalvaFuncionarioLogica implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		String cpf = req.getParameter("cpf");
		String senha = req.getParameter("senha");

		
		// cria uma inst�ncia de contato com os dados
		// do formul�rio
		Funcionario funcionario = new Funcionario();
		funcionario.setNome(nome);
		funcionario.setEmail(email);
		funcionario.setCpf(cpf);
		funcionario.setSenha(senha);
		
		// cria uma inst�ncia de dao e abre uma
		// conex�o com o banco de dados
		FuncionarioDao dao = new FuncionarioDao();
		
		// chama o m�todo para salvar o contato
		dao.salva(funcionario);
		
		// feedback para o usu�rio
		return "/WEB-INF/jsp/funcionario-adicionado.jsp";
	}

}