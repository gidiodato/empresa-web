/**
 * @author Giovanna Ferreira Diodato
 */
package br.senai.sp.informatica.empresaweb.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresaweb.dao.FuncionarioDao;
import br.senai.sp.informatica.empresaweb.model.Funcionario;

public class AtualizaFuncionarioLogica implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {

		// Recuperar o id
		long id = Long.parseLong(req.getParameter("id"));

		// Recupera os dados do form
		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		String cpf = req.getParameter("cpf");
		String senha = req.getParameter("senha");

		// Obt�m uma inst�ncia de Contato
		Funcionario funcionario = new Funcionario();

		// Inicializa os atributos de contato
		funcionario.setId(id);
		funcionario.setNome(nome);
		funcionario.setEmail(email);
		funcionario.setCpf(cpf);
		funcionario.setSenha(senha);

		// Obt�m uma inst�ncia de dao de contato
		FuncionarioDao dao = new FuncionarioDao();

		// Salva o contato
		dao.salva(funcionario);

		// Mostra a lista de contatos atualizada
		return "mvc?logica=ListaFuncionariosLogica";

	} // Fim do executa
} // fim da classe