/**
 * @author Giovanna Ferreira Diodato
 */
package br.senai.sp.informatica.empresaweb.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AdicionaFuncionarioLogica implements Logica {
	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
	
		return "WEB-INF/jsp/adiciona-funcionario.jsp";
		
	}

}
