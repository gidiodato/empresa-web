/**
 * @author Giovanna Ferreira Diodato
 */
package br.senai.sp.informatica.empresaweb.mvc.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresaweb.mvc.logica.Logica;

// http://localhost:8080/mvc
@WebServlet("/mvc")
public class ControllerServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// obtem o parametro logica da requisicao
		// http://localhost:8080/mvc?logica=xpto
		String parametro = req.getParameter("logica");

		// compoe o nome da classe de logica utilizando o parametro
		// br.senai.informatica.empresaweb.mvc.logica.xpto
		String className = "br.senai.sp.informatica.empresaweb.mvc.logica." + parametro;
		try {
			// carrega a classe para a mem�ria
			// br.senai.sp.informatica.empresaweb.mvc.logica
			Class classe = Class.forName(className);

			// cria uma nova instamcia da classe de l�gica que est� na mem�ria
			Logica logica = (Logica) classe.newInstance();

			// executa o metidi da classe de logica que foi passada
			// xpto.executa(req, res)
			// o m�todo executa deve retornar um jsp, e o dispatcher
			// deve encaminhar o usu�rio para esse jsp
			String pagina = logica.executa(req, res);

			// obtem o request dispatcher passando pra ele a p�gina
			// retornada do m�todo
			// executa e encaminha o usuarios
			req.getRequestDispatcher(pagina).forward(req, res);
		} catch (Exception e) {
			throw new ServletException("A l�gica de neg�cios causou um exce��o: ", e);
		}

	}

}
