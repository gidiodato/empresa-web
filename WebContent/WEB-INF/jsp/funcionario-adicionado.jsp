<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon"
	href="https://icon-icons.com/icons2/495/PNG/512/user-check_icon-icons.com_48405.png">
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/estilo.css">
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<title>Sucesso...</title>
</head>
<body style="background-color: #9774AB">
	<header>
	<div id="principal" style="margin-bottom: 0px"
		class="jumbotron centro jumbotron-fluid font-color">
		<div class="container">
			<h1 class="display-3">Funcionário Cadastrado!</h1>
		</div>
	</div>
	</header>

	<div class="container">
		<div class="conteudo centro font-color">
			<input type="submit" class="btn btn-primary"
				value="Adicionar Novo Funcionário"
				onclick="location.href = 'mvc?logica=AdicionaFuncionarioLogica';">
			<input type="submit" class="btn btn-primary"
				value="Ver Lista de Funcionários"
				onclick="location.href = 'mvc?logica=ListaFuncionariosLogica';">
		</div>
	</div>

	<div class="font-color passar">
		<hr>
		Copyright&copy; 2017 - Todos os direitos reservados
	</div>
</body>
</html>