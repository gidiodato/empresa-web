<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<title>Cadastro de Funcion�rios</title>
<link rel="icon"
	href="https://icon-icons.com/icons2/495/PNG/512/cog-box_icon-icons.com_48654.png">
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/estilo.css">
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>

</head>

<body style="background-color: #9774AB">
	<header>
		<div id="principal" style="margin-bottom: 0px"
			class="jumbotron centro jumbotron-fluid font-color">
			<div class="container">
				<h1 class="display-3">Seja Bem-Vindo!</h1>
				<p class="lead">Atualize os seus dados!</p>
			</div>
		</div>
	</header>

	<div class="container">
		<div class="conteudo centro font-color">
			<h2>Digite seus Novos Dados</h2>
		</div>

		<!--Come�o do formul�rio-->
		<form
			action="mvc?logica=AtualizaFuncionarioLogica&id=${requestScope.id }"
			method="post" />
		<!--Campo de primeiro nome-->
		<div class="form-group">
			<label for="primeiroNome" class="font-color">Primeiro Nome</label> <input
				type="text" name="nome" value="${requestScope.nome }"
				class="form-control" id="primeiroNome"
				placeholder="Informe seu primeiro nome">
		</div>

		<!--Campo de Email-->
		<div class="form-group">
			<label for="email" class="font-color">Email</label> <input
				type="text" name="email" value="${requestScope.email }"
				class="form-control" id="email" placeholder="Digite seu email">
		</div>

		<!--Campo de CPF-->
		<div class="form-group">
			<label for="cpf" class="font-color">CPF</label> <input type="text"
				name="cpf" value="${requestScope.cpf }" class="form-control"
				id="cpf" placeholder="Digite seu CPF">
		</div>

		<!--Campo de Senha-->
		<div class="form-group">
			<label for="senha" class="font-color">Senha</label> <input
				type="password" name="senha" value="${requestScope.senha }"
				class="form-control" id="senha" placeholder="Digite sua senha">
		</div>
		<div class="centro">
			<button type="submit" name="Salvar" class="btn btn-primary">Enviar
				Dados</button>
		</div>
	</div>
	</form>

	<div class="font-color passar">
		<hr>
		Copyright&copy; 2017 - Todos os direitos reservados
	</div>

</body>
</html>