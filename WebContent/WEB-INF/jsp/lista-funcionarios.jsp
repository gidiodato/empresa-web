<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%-- cabeçalho da taglib core --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon"
	href="https://icon-icons.com/icons2/37/PNG/512/addthelist_a%C3%B1adir_3477.png">
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/estilo.css">
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<title>Lista de Funcionários</title>
</head>

<body style="background-color: #9774AB">

	<header>
	<div id="principal" style="margin-bottom: 0px"
		class="jumbotron centro jumbotron-fluid font-color">
		<div class="container">
			<h1 class="display-3">Lista de Funcionários</h1>
		</div>
	</div>
	</header>

	<div class="container font-color">
		<table style="margin-top: 40px" class="table table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nome</th>
					<th>CPF</th>
					<th>Email</th>
					<th colspan="2">Opções</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="funcionario" items="${funcionarios }" varStatus="id">
					<tr>
						<td>${id.count }</td>
						<td>${funcionario.nome }</td>
						<td>${funcionario.cpf}</td>
						<td><c:if test="${not empty funcionario.email }">
								<a href="mailto:${funcionario.email}"> ${funcionario.email }
								</a>
							</c:if> <c:if test="${empty funcionario.email}">
					E-mail não informado
				</c:if></td>
						<td><a
							href="mvc?logica=RemoveFuncionarioLogica&id=${funcionario.id }">Excluir</a>
						</td>
						<td><a
							href="mvc?logica=ExibeFuncionarioLogica&id=${funcionario.id }">Atualizar</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

	<div class="container">
		<div class="conteudo centro font-color">
			<input type="submit" class="btn btn-primary"
				value="Adicionar Novo Funcionário"
				onclick="location.href = 'mvc?logica=AdicionaFuncionarioLogica';">
		</div>
	</div>

	<div class="font-color passar">
		<hr>
		Copyright&copy; 2017 - Todos os direitos reservados
	</div>
</body>
</html>